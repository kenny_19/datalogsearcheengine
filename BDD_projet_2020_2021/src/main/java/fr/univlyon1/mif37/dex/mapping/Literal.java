package fr.univlyon1.mif37.dex.mapping;

//with tgd subgoal target side
public class Literal {
  // autom(predicat ou subgoal) is one of member in querery side
  private Atom a;
  //marking argument in autom(predicat ou subgoal) is free (true)
  //qui est contre que adorned
  private Boolean flag;

  public Literal(Atom a, Boolean flag) {
    this.a = a;
    this.flag = flag;
  }

  public Boolean getFlag() {
    return flag;
  }
  
  public Atom getAtom() {
    return a;
  }

  public boolean containsVariable(Variable v) {
    return a.getVars().contains(v);
  }

  /*ajouter pour bien afficher*/
  @Override
  public String toString() {
    return "{atom =" + a +
            ", flag=" + flag +
            '}';
  }
}
