package fr.univlyon1.mif37.dex.mapping.topDown;
import fr.univlyon1.mif37.dex.mapping.Value;
import java.util.List;

/**
 * creer pour stock les ruled adornd
 * Input relation and supplentaire relation
 */
public class TermSchema {
    //ruel
   public AdornedTgd rule;
   //input relation
   public Relation inputR;
   //raltion
   public QsqTemplate suppR;

    public TermSchema(AdornedTgd rule, Relation inputR, QsqTemplate suppR) {
        this.rule = rule;
        this.inputR = inputR;
        this.suppR = suppR;
    }

    public AdornedTgd getRule() {
        return rule;
    }

    public void setRule(AdornedTgd rule) {
        this.rule = rule;
    }

    public Relation getInputR() {
        return inputR;
    }

    public void setInputR(Relation inputR) {
        this.inputR = inputR;
    }

    public QsqTemplate getSuppR() {
        return suppR;
    }

    public void setSuppR(QsqTemplate suppR) {
        this.suppR = suppR;
    }
}
