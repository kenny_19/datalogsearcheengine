package fr.univlyon1.mif37.dex.mapping;

import java.util.*;

//autom(predicat ou subgoal)
//peut utilise source ou target
public class Atom {
    //name of predicate
    private String name;
    //argument of subgoal with two type value or constant
    private Value[] args;

    public Atom(String name, List<Value> args) {
        this.name = name;
        this.args = args.toArray(new Value[args.size()]);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Value[] getArgs() {
        return args;
    }

    //obtient tous les parametre en type variable
    public Collection<Variable> getVars() {
      Collection<Variable> container = new ArrayList<Variable>();
      for (Value v: this.getArgs()) {
        if (v instanceof Variable) {
          container.add((Variable)v);
        }
      }
      return container;
    }

    //getValue en tab
    public Value[] getVarsEnTab(){
        return args;
    }

    @Override
    public String toString() {
        String result = "";
        result += name + "(";
        int i = 1;
        for(Value v : args) {
            result += v;
            if(i  != args.length)
                result += ",";
            ++i;
        }
        result += ")";
        return  result;
    }

    //predicat est equal
    //tous les param est eagle
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atom atom = (Atom) o;
       if(atom.getName().equals(name)){
           Value argsA[]= atom.getVarsEnTab();
           if(args.length==argsA.length){
               for(int i=0;i<argsA.length;i++){
                   if(!argsA[i].getName().equals(args[i].getName())){
                       return false;
                   }
                   else
                       return true;
               }
           }
       }
       return false;
    }

    /**
    * obtient une value d'apres indice
    * */
    public Value chercheValue(int i){
        return args[i];
    }
}
