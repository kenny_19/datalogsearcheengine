package fr.univlyon1.mif37.dex.mapping.topDown;

import fr.univlyon1.mif37.dex.mapping.Atom;
import fr.univlyon1.mif37.dex.mapping.Mapping;
import fr.univlyon1.mif37.dex.mapping.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @juba BDD
 */
/**
 * A template containing the attribute schemata of the supplementary relations
 * for a given rule in QSQ evaluation.
 *
 */
/* 包含补充关系属性图式的模板
 * 对于 QSQ 评估中的给定规则。 */
public class QsqTemplate {
    /**
     * The attribute schemata for the supplementary relations.
     */
    /*
    * 补充关系的属性图式
    * private  List<TermSchema> schemata;
    * */
    private ArrayList<AdornedAtom> Body;

    private ArrayList<Tuple> t;

    private int indiceRule;

    private AdornedAtom Head;

    private ArrayList<AdornedAtom> listAtom;

    /**
     * Constructs a template from an adorned rule.
     *
     * @param rule
     *            the adorned rule
     */
    /*从装饰规则构造模板。 */
    public QsqTemplate(AdornedTgd rule) {
        listAtom = rule.copyEnList();
        Body =new ArrayList<>();
        t = new ArrayList<>();
        for(int i=0;i<listAtom.size();i++){
            if(i!=listAtom.size()-1){
                Body.add(listAtom.get(i));
            }else{
                Head = listAtom.get(i);
            }
        }
    }

    public QsqTemplate(QsqTemplate q) {
        this.t= new ArrayList<>();
        this.Head=q.Head;
        this.Body=q.Body;
        this.listAtom=q.listAtom;
        this.indiceRule=q.indiceRule;
        for(int i=0;i<q.t.size();i++){
            t.add(q.t.get(i).copy());
        }
    }



    public ArrayList<AdornedAtom> getListAtom() {
        return listAtom;
    }

    public void setListAtom(ArrayList<AdornedAtom> listAtom) {
        this.listAtom = listAtom;
    }

    public ArrayList<AdornedAtom> getBody() {
        return Body;
    }

    public void setBody(ArrayList<AdornedAtom> body) {
        Body = body;
    }

    public AdornedAtom getHead() {
        return Head;
    }

    public void setHead(AdornedAtom head) {
        Head = head;
    }

    public ArrayList<Tuple> getT() {
        return t;
    }

    public void setT(ArrayList<Tuple> t) {
        this.t = t;
    }

    public int getIndiceRule() {
        return indiceRule;
    }

    public void setIndiceRule(int indiceRule) {
        this.indiceRule = indiceRule;
    }

    //projection tuple
    //il faut verifier faire en premier
    //etape 2 partie 4
    public void tupleEnProjection(Relation newInput){
        HashMap<AdornedAtom,Tuple> a=newInput.getAtomAd();
        Tuple tepT;
        Tuple tempTuple=new Tuple();
        AdornedAtom tmpA;
        //遍历hashMap
        for(Map.Entry<AdornedAtom,Tuple> entry : a.entrySet()){
            tepT = entry.getValue();
            tmpA = entry.getKey();
            //tester si c'est le rule qui a la tete correspond atom dans input
            if(Head.getAtom().getName().equals(tmpA.getAtom().getName())){
                AdornedAtom premierBody = Body.get(0);
                ArrayList<Boolean> adornmentBodyAtom= (ArrayList<Boolean>) premierBody.getAdornment();
                int nbArg=adornmentBodyAtom.size();
                //parcours param de premier atom
                for(int i=0;i<nbArg;i++){
                    //si une varibale bound
                    if(adornmentBodyAtom.get(i)){
                        //si ce tuple existe dans input relation on le ajoute dans premier relation supp
                        if(tepT.chercheValue(premierBody.getAtom().getArgs()[i])){
                            tempTuple.ajouterUnElement(premierBody.getAtom().getArgs()[i],tepT.chercheValeurValue(premierBody.getAtom().getArgs()[i]));
                        }
                    }
                }
                //pour projection requete lui meme
            }else{
                //gagne indice rule
                if(indiceRule==newInput.getIndiceRule()){
                    AdornedAtom premierBody = Body.get(0);
                    ArrayList<Boolean> adornmentBodyAtom= (ArrayList<Boolean>) premierBody.getAdornment();
                    int nbArg=adornmentBodyAtom.size();
                    //parcours param de premier atom
                    for(int i=0;i<nbArg;i++){
                        //si une varibale bound
                        if(adornmentBodyAtom.get(i)){
                            //si ce tuple existe dans input relation on le ajoute dans premier relation supp
                            if(tepT.chercheValue(premierBody.getAtom().getArgs()[i])){
                                tempTuple.ajouterUnElement(premierBody.getAtom().getArgs()[i],tepT.chercheValeurValue(premierBody.getAtom().getArgs()[i]));
                            }
                        }
                    }
                }
            }
        }
        //System.out.println(tempTuple);
        //System.out.println(tempTuple.elts.size());
        t.add(tempTuple);
    }

    @Override
    public String toString() {
        return "QsqTemplate{" +
                "Body=" + Body +
                "\n , t=" + t +
                ", indiceRule=" + indiceRule +
                "\n , Head=" + Head +
                "\n , listAtom=" + listAtom +
                '}';
    }

    //genere le supp mais pas de premier et aussi pour atom edb
    public  ArrayList<Tuple> genSupp(int lieuSupp, Mapping m) {
        ArrayList<Tuple>res=new ArrayList<>();
        if(lieuSupp!=0){
            //获取上一项
            Tuple tmp=t.get(lieuSupp-1);
            //判断上一个补充关系存在
            if(tmp!=null && tmp.elts.size()!=0){
                //获取上一个adorned atom
                AdornedAtom a = listAtom.get(lieuSupp-1);
                //获取绑定值的位置信息
                Atom atom =a.getAtom();
                Value[]listVal= atom.getArgs();
                ArrayList<Integer>listPositionBound=new ArrayList<>();
                for(int i=0;i<listVal.length;i++){
                    if(tmp.chercheValue(listVal[i])){
                        listPositionBound.add(i);
                    }
                }
                //判定绑定的值的情况
                //若有绑定则继续，若无绑定则结束
                if(listPositionBound.size()!=0){
                    //能找到新的则idb
                    boolean typeAtomEDB=m.chercheConstant(atom.getName());
                    //boolean typeAtomIDB=m.chercheIbd(atom.getName());
                    //判断该项类型 edb则寻找新的值
                    if(typeAtomEDB){
                        //绑定的位置
                        int lieuFix = listPositionBound.get(0);
                        //固定的变量对应的值
                        String val=tmp.elts.get(atom.getArgs()[lieuFix]);

                        //默认一个绑定值，一共两个值，那么去寻找第二个值。
                        if(atom.getArgs().length>=2){
                            ArrayList<String>valeurPossible=m.chercheTousConstant(a.getPredicat(),1-lieuFix,lieuFix,val);
                            Value nouvValue=atom.getArgs()[1-lieuFix];
                            //System.out.println(valeurPossible.size());
                            if(valeurPossible.size()!=0){
                                //对每个新绑定的值分别做出分支讨论
                                for(int i=0;i<valeurPossible.size();i++){
                                    //分别生成新的supp relation
                                    //每个新的relation做一个分支存入结果
                                    Tuple tupleTemp=new Tuple();
                                    //res.put(valeurPossible.get(i),nouvValue);
                                    tupleTemp.ajouterUnElement(nouvValue,valeurPossible.get(i));
                                    //System.out.println(tupleTemp);
                                    res.add(tupleTemp);
                                }
                            }
                        }else{
                            //si param est unique
                            if(atom.getArgs().length==1){
                                ArrayList<String>valeurPossible=m.chercheTousConstant(a.getPredicat(),0,0,val);
                                Value nouvValue=atom.getArgs()[0];
                                //System.out.println(valeurPossible.size());
                                if(valeurPossible.size()!=0){
                                    //对每个新绑定的值分别做出分支讨论
                                    for(int i=0;i<valeurPossible.size();i++){
                                        //分别生成新的supp relation
                                        //每个新的relation做一个分支存入结果
                                        Tuple tupleTemp=new Tuple();
                                        //res.put(valeurPossible.get(i),nouvValue);
                                        tupleTemp.ajouterUnElement(nouvValue,valeurPossible.get(i));
                                        //System.out.println(tupleTemp);
                                        res.add(tupleTemp);
                                    }
                                }
                            }
                        }

                    }

                }
            }

        }
        return res;
    }

    /**
     * copy
     * */
    public QsqTemplate copy(){
        return new QsqTemplate(this);
    }
    /**
     * ajouter la relation complet dans list tuple
     * */
    public void ajouterTuple(Tuple tmp){
        HashMap<Value,String> elts=new HashMap<>();
        elts.putAll(tmp.getElts());
        t.add(new Tuple(elts));
    }
}
