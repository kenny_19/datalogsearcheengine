package fr.univlyon1.mif37.dex.mapping;

//parametre dans subgoal de IDB soit constant soit variable
//comme preeache et x dans reachable(preeache,x)
public class AbstractArgument {
  
  private String attribute = null;
  private Variable variable = null;

  public AbstractArgument(String attribute) {
    this.attribute = attribute;
  }

  public AbstractArgument(Variable v) {
    this.variable = v;
  }
  
  public boolean isVariable() {
    return variable != null;
  }
  
  public boolean isAttribute() {
    return attribute != null;
  }

  public Variable getVar() {
    return variable;
  }
  
  public String getAtt() {
    return attribute;
  }

  @Override
  public String toString() {
    return variable.toString();
  }
}
