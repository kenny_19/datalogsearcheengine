package fr.univlyon1.mif37.dex.mapping;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Multimap;
/*
import com.sun.org.apache.xpath.internal.operations.Mult;
import com.sun.org.apache.xpath.internal.SourceTree;
*/

import java.util.*;
/*
* contient trois partie
* edb ide et tgds
* */
public class Mapping {
    //Collection-arrayList
    private Collection<Relation> edb;

    private Collection<AbstractRelation> idb;

    private Collection<Tgd> tgds;

    public Mapping() {
        this.edb = new ArrayList<>();
        this.idb = new ArrayList<>();
        this.tgds = new ArrayList<>();
    }

    public Mapping(Mapping mapping) {
        this.edb = new ArrayList<>(mapping.getEDB());
        this.idb = new ArrayList<>(mapping.getIDB());
        this.tgds = new ArrayList<>(mapping.getTgds());
    }

    public Mapping(Collection<Relation> edb, Collection<AbstractRelation> idb, Collection<Tgd> tgds) {
        this.edb = edb;
        this.idb = idb;
        this.tgds = tgds;
    }

    public Collection<Relation> getEDB() {
        return edb;
    }

    public Collection<AbstractRelation> getIDB() {
        return idb;
    }

    public Collection<Tgd> getTgds() {
        return tgds;
    }

    /*retourner Tgds que on interesse*/
    public Atom chercheQuery(int i){
        if(i<tgds.size()){
            ArrayList <Tgd> tmp= (ArrayList)tgds;
            Tgd res = tmp.get(i);
            return res.getRight();
        }
        return null;
    }

/**
*   Instiate the Datalog Top Down Engine in this part
*
**/


    public String toString() {
        String result = "";
        result += "EDB"+ "\n";
        for(Relation r : this.getEDB())
            result += r+ "\n";
        result +=""+ "\n";
        result +="IDB"+ "\n";
        for(AbstractRelation relation : this.getIDB())
            result +=relation+ "\n";
        result +=""+ "\n";;
        result +="TGD"+ "\n";
        for(Tgd tgd : this.getTgds())
            result +=tgd + "\n";
        return result;
    }

    /*
     *   cherche un predicat dans edb.
     *   si existe true
    */
    public  Boolean chercheConstant(String nomPredicat){
        for(fr.univlyon1.mif37.dex.mapping.Relation i:edb){
            if(nomPredicat.equals(i.getName())){
                return true;
            }
        }
        return false;
    }

    /*
    * cherche un predicat dans edb.
    * avec une valeur constant en String.
    * */
    public  String chercheConstant(String nomPredicat,int indiceParam){
        if(chercheConstant(nomPredicat)){
            for(fr.univlyon1.mif37.dex.mapping.Relation i:edb){
                if(nomPredicat.equals(i.getName())){
                    return i.chercheConstant(indiceParam);
                }
            }
        }

        return new String("");
    }

    public boolean chercheIbd(String nomPredicat){
        for(fr.univlyon1.mif37.dex.mapping.AbstractRelation i:idb){
            if(nomPredicat.equals(i.getName())){
                return true;
            }
        }
        return false;
    }

    //寻找所有的const所在的Relation
    public ArrayList<Relation> cercherEdbList(String nomPredicat){
        //到所有的该predicat下的edb相应的参数
        ArrayList<Relation>res=new ArrayList<>();
        for(Relation r:edb){
            if(r.getName().equals(nomPredicat)){
                res.add(r);
            }
        }
        return res;
    }
    //cherche tous les constant parmi une position avec valeur fix
    public ArrayList<String> chercheTousConstant(String nomPredicat,int indice,int indiceFix,String valeurFix){
        ArrayList<String>res=new ArrayList<>();
        ArrayList<Relation> listC=cercherEdbList(nomPredicat);
        for(Relation i :listC){
            if(i.attributes.length>indiceFix&& i.attributes.length>indice){
                if(i.attributes[indiceFix].equals(valeurFix))
                    res.add(i.attributes[indice]);
            }
        }
        return res;
    }


}
