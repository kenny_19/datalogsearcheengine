package fr.univlyon1.mif37.dex.mapping.topDown;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Adler32;
/**
 * @juba BDD
 */
/**
 * An adorned tgd (i.e., a Horn clause where every atom is itself adorned).
 *
 */
/*一个 Horn 子句，其中每个原子都有自己的装饰 */
public class AdornedTgd {

    private final AdornedAtom head;

    private final List<AdornedAtom> body;

    /**
     * Constructs an adorned tgd given an adorned atom for the head and a
     * list of adorned atoms for the body.
     *
     * @param head
     *            head atom of clause
     * @param body
     *            atoms for body of clause
     */
    public AdornedTgd(AdornedAtom head, List<AdornedAtom> body) {
        this.head = head;
        this.body = body;
    }

    /*code getter*/
    public AdornedAtom getHead() {
        return head;
    }

    public List<AdornedAtom> getBody() {
        return body;
    }

    /*
    * obtient un atom adorned par index
    * sinon rt null
    * */
    public AdornedAtom getAtom(int i){
        if(i<body.size()+1){
            if(i==body.size())
                return getHead();
            else
                return body.get(i);
        }
        return null;
    }


    @Override
    public String toString() {
        return "AdornedTgd{" +
                ", body=" + body +
                "head=" + head +
                '}';
    }

    /**
    * rendu comme une list de AdornedAtom
    * la tete est la dernier element
    */
    public ArrayList<AdornedAtom> copyEnList(){
        ArrayList<AdornedAtom>res=new ArrayList<>();
        for(AdornedAtom i:body){
            res.add(i);
        }
        res.add(head);
        return res;
    }
}
