package fr.univlyon1.mif37.dex.mapping.topDown;

import fr.univlyon1.mif37.dex.mapping.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @juba BDD
 */

/**
 * A tuple of terms, i.e., an ordered list of fixed arity.
 *
 */
/*
* 术语元组，即固定元数的有序列表。
 * */
public class Tuple {
    /**
     * The value in this tuple.
     */
    //此元组中的值。
    public HashMap<Value,String> elts;

    /**
     * Constructs a tuple from a list of values.
     *
     * @param elts
     *            the list of values
     */
    //从值列表构造元组。
    public Tuple(HashMap<Value,String> elts) {
        this.elts = elts;
    }

    public Tuple(Tuple t){
        this.elts=t.elts;
    }

    public Tuple(){
        elts=new HashMap<>();
    }

    /*getter et setter*/

    public HashMap<Value, String> getElts() {
        return elts;
    }

    public void setElts(HashMap<Value, String> elts) {
        this.elts = elts;
    }

    //tester element value dans ce tuple
    public Boolean chercheValue(Value v){
        for (Map.Entry<Value,String> entry : elts.entrySet()) {
            if(entry.getKey().equals(v)){
                return true;
            }
        }
        return false;
    }

    //cherche valeur pour element value dans ce tuple
    public String chercheValeurValue(Value v){
        for (Map.Entry<Value,String> entry : elts.entrySet()) {
            if(entry.getKey().equals(v)){
                return entry.getValue();
            }
        }
        return new String("");
    }

    //public void ajouter un element dans tuple
    public void ajouterUnElement(Value v,String s){
        elts.put(v,s);
    }

    @Override
    public String toString() {
        return "Tuple{" +
                "elts=" + elts +
                '}';
    }

    //copy
    public Tuple copy(){
        HashMap<Value,String> elts=new HashMap<>();
        elts.putAll(this.elts);
        return new Tuple(elts);
    }
}
