package fr.univlyon1.mif37.dex.mapping.topDown;

import fr.univlyon1.mif37.dex.mapping.Value;

import java.util.*;
/**
 * @juba BDD
 */
/**
 * A relation, i.e., a set of tuples  with an associated
 * attribute schema of the same arity.
 *
 * Utiliser pour relation input
 * 用于input
 */
public class Relation {
    /**
     * The tuples of this relation.
     */
    //这种关系的元组。
    protected Set<Tuple> tuples;
    /**
     * The attribute schema of this relation.
     */
    //此关系的属性模式。
    //protected TermSchema attributes;
    protected HashMap<AdornedAtom,Tuple>  atomAd;

    //ordre dans list rule
    protected int indiceRule;



    /**
     * Constructs an empty relation
     */
    //构造一个空关系
    public Relation (){
        // avant step 1 partie 4
        // We initialize all auxiliary relations to empty sets.
        tuples = new HashSet<>();
        //attributes = new TermSchema(new ArrayList<>());
        atomAd = new HashMap<>();
    }

    public Relation(Relation r){
        indiceRule=r.indiceRule;
        tuples = new HashSet<>();
        if(r.tuples!=null) {
            for (Tuple i : r.tuples) {
                tuples.add(i.copy());
            }
        }
        atomAd = new HashMap<>();
       if(r.atomAd!=null){
           Tuple tmp;
           for (Map.Entry<AdornedAtom,Tuple> entry : r.atomAd.entrySet()) {
               AdornedAtom key=entry.getKey();
               Tuple v=entry.getValue();
               tmp=v.copy();
               atomAd.put(key,tmp);
           }
       }

    }

    /*les getter et setter pour valeur privie*/
    public Set<Tuple> getTuples() {
        return tuples;
    }

    public void setTuples(Set<Tuple> tuples) {
        this.tuples = tuples;
    }

    public void setTuples(ArrayList<Tuple> tuples) {
        Set<Tuple>res=new HashSet<>();
        for(Tuple i:tuples){
            res.add(i);
        }
        this.tuples=res;
    }

    public void ajouterUnTuple(Tuple t){
        this.tuples.add(t);
    }


    public HashMap<AdornedAtom, Tuple> getAtomAd() {
        return atomAd;
    }

    public void setAtomAd(HashMap<AdornedAtom, Tuple> atomAd) {
        this.atomAd = atomAd;
    }

    public void ajouterDansAtomAd(AdornedAtom ad, Tuple t){
        atomAd.put(ad,t);
    }

    //tester element existe dans set tuple
    public Boolean chercheValue(Value v){
        for(Tuple t:tuples){
            if(t.chercheValue(v))
                return true;
        }
        return false;
    }

    //cherche une varible existe  dans Tuple c.a.d les variable de tuple existe dans collection Tuple
    public Boolean chercheTupleIBD(Value t){
        for(Tuple i:tuples){
            if(i.chercheValue(t))
                return true;
        }
        return false;
    }

    //cherche une valeur sur ce variable
    public String chercheTupleIBDVal(Value t){
        if(chercheTupleIBD(t)){
            for(Tuple i:tuples){
                if(i.chercheValue(t))
                    return i.chercheValeurValue(t);
            }
        }
        return new String("");
    }

    public int getIndiceRule() {
        return indiceRule;
    }

    public void setIndiceRule(int indiceRule) {
        this.indiceRule = indiceRule;
    }

    //cherche un tuple existe dans set tuple
    public boolean chercheTupleExiste(Tuple t){
        return tuples.contains(t);
    }

    /***
     * tester les un relation sont contient dans ce cas
     *
     */
    public boolean testeContient(Relation r){
        HashMap<AdornedAtom,Tuple>a1 = r.getAtomAd();
        ArrayList<Boolean>elementDjaExit=new ArrayList<>();
        for (Map.Entry<AdornedAtom,Tuple> entryEntree : a1.entrySet()) {
            //System.out.println("boucle");
            AdornedAtom adatom1 = entryEntree.getKey();
            //System.out.println("adatom1 : "+adatom1.getAtom().getName());
            Tuple tuple1 = entryEntree.getValue();
            List<Boolean> listBool1= adatom1.getAdornment();

            for (Map.Entry<AdornedAtom,Tuple> entryOrigin : atomAd.entrySet()) {

                AdornedAtom adatom2 = entryOrigin.getKey();
                //System.out.println("adatom2 : "+adatom2.getAtom().getName());
                Tuple tuple2 = entryOrigin.getValue();
                List<Boolean> listBool2= adatom2.getAdornment();
                int nbBound=0;
                //si la meme predicat
                if(adatom2.getAtom().getName().equals(adatom1.getAtom().getName())){
                    //teste tous les variable bound sont egale avec son position indice
                    //parcour tuple et
                    //System.out.println("meme predicat");
                    if(listBool1.size()==listBool2.size()){
                        //parcours tous les parametre
                        for(int i=0;i<listBool1.size();i++){
                            Value v1=adatom1.getVarDansAtom(i);
                            Value v2=adatom2.getVarDansAtom(i);
                            String val1 = tuple1.chercheValeurValue(v1);
                            String val2 = tuple2.chercheValeurValue(v2);
                            System.out.println(tuple1);
                            System.out.println(tuple2);
                            System.out.println(" v1 : " + v1 + " val1 "+val1);
                            System.out.println(" v2 : " + v2 + " val1 "+val2);
                            //compter le nb de param qui as meme valeurs
                            //空的也有可能相等，但是不该考虑
                            if(!val1.equals("")&&val1.equals(val2)){
                                nbBound++;
                            }
                        }
                    }
                    if(nbBound==adatom1.getBound()){
                        //deja trouve un atom input correspond
                        elementDjaExit.add(true);
                    }
                    //System.out.println(nbBound);
                    //System.out.println(adatom1.getBound());
                }
            }

        }

        System.out.println("elementDjaExit.size"+elementDjaExit.size());
        System.out.println("a1.size"+a1.size());
        if(elementDjaExit.size()!=a1.size()){
            return false;
        }else{
            return true;
        }
    }

    public Relation copy(){
        return new Relation(this);
    }
    /**
     * renouvler valeur bouding pour une variable
     * et puis passer nouveuax valeur pour les rest varible qui a le mem nom
     * */
    public void renouvoulerAtomAd(String predicat,Tuple nouvTuple){
        AdornedAtom a=null;
        AdornedAtom b=null;
        for (Map.Entry<AdornedAtom,Tuple> entry : atomAd.entrySet()) {
            if(entry.getKey().getAtom().getName().equals(predicat)){
                a=entry.getKey();
            }
        }
        //renouvouler
        if(a!=null){
            atomAd.remove(a);
            atomAd.put(a,nouvTuple);
            //renouvouler aussi la premier atom

            for (Map.Entry<AdornedAtom,Tuple> entry : atomAd.entrySet()) {
                if(!(entry.getKey().getAtom().getName().equals(predicat))){
                    b=entry.getKey();
                }
            }
            if(b!=null){
                atomAd.remove(b);
                atomAd.put(b,nouvTuple);
            }

        }
    }


    @Override
    public String toString() {
        return "Relation{" +
                "tuples=" + tuples +
                ", atomAd=" + atomAd +
                '}';
    }
}
