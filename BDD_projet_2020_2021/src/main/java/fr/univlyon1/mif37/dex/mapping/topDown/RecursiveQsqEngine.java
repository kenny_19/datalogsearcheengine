package fr.univlyon1.mif37.dex.mapping.topDown;

import fr.univlyon1.mif37.dex.mapping.*;

import java.util.*;
/**
 * @juba BDD
 */
/**
 * A Datalog evaluation engine that uses a recursive version of the
 * query-subquery top-down technique.
 *
 */
public class RecursiveQsqEngine {


    //public static Object QSQRState1;
    public QSQRState QSQRState1;
    /*pour bien utilise et plus pratique*/
    public Mapping bdd;


    public RecursiveQsqEngine(Mapping bdd) {
        this.bdd = bdd;
        ArrayList<Tgd> tgdOrigin= (ArrayList)bdd.getTgds();
        Map < Object,Object>unadornedRules = new LinkedHashMap();
        Tgd tempT;
        for(int i=0;i<tgdOrigin.size();i++){
            tempT = tgdOrigin.get(i);
            unadornedRules.put(tempT.getLeft(),tempT.getRight());
        }
        //creer adorned rule
        iniIterateur(unadornedRules,bdd.getEDB());
    }

    /**
     *
     *
     * Preparation of query q and retun the obtained result
     * 准备查询q并返回得到的结果
     *
    **/
    public Set<Object> query(Atom q) {
        Set<Object> result = new LinkedHashSet<>();
        AdornedAtom a = QSQRState1.chercheAdornedAtom(q);
        System.out.println(q);
        Relation newInput=chercheNewInput(q);
        //premier input utilise jamais， ajouter dans canal de stock
        //第一个input，未用过 压入input 历史
        if(!QSQRState1.inputDansHis(newInput)){
            QSQRState1.ajouterHisatory(newInput);
            System.out.println(QSQRState1.getHistoryInput());
            //System.out.println(QSQRState1.getindiceAdTgd(q));
            if(a !=null){
                qsqr(a,newInput, this.QSQRState1);
            }
        }
        return result;
    }



    /**
     * Evaluates the query represented by the adorned predicate p and the
     * relation newInput.
     *
     * @param p
     *            adorned predicate of query
     * @param newInput
     *            input tuples
     * @param state
     *            current state of evaluation-wide variables
     *            评估由修饰谓词 p 和 关系新输入。
     */
    private void qsqr(AdornedAtom p, Relation newInput, QSQRState state) {
        //System.out.println(newInput);
        ArrayList<AdornedTgd>arryRule = QSQRState1.getAdTgd();
        //System.out.println(QSQRState1.getindiceAdTgd(p.atom));
        //parcour pour tous les rules
        for(int i=0;i<arryRule.size();i++){
            //etpe 2 part 4
            //pour le rule rest
            //if(newInput.getIndiceRule()!=i){
                //faire projection et creer premier rule
                qsqrSubroutine(arryRule.get(i), newInput, state,i,QSQRState1.getindiceAdTgd(p.atom));
            //}
        }

    }

    /**
     * Evaluates the supplied rule using the input tuples newInput.
     *
     * @param rule
     *            rule
     * @param newInput
     *            input tuples
     * @param state
     *            current state of evaluation-wide variables
     * @param indiceRule
     *            ordre de rule
     */
    /*
    * 使用输入元组 newInput 评估提供的规则。
    * etpe 2 part 4 : projection variable vient de projection
    * */
    private void qsqrSubroutine(AdornedTgd rule, Relation newInput, QSQRState state,int indiceRule,int queryOrigin) {
        QsqTemplate temp = new QsqTemplate(rule);
        temp.setIndiceRule(indiceRule);
        int lieuSupp=0;
        //generer premier complementaire
        //生成新的映射
        temp.tupleEnProjection(newInput);

        lieuSupp++;
        //si l'atom est la type de edb genrer la supp complementaire suivant
        Atom a=temp.getListAtom().get(0).atom;
        boolean typeAtomEDB=bdd.chercheConstant(a.getName());
        if(typeAtomEDB){
            ArrayList<Tuple> nouvValDansRelationSub = temp.genSupp(lieuSupp, bdd);
            //System.out.println(nouvValDansRelationSub);

            //是否是body的末端
            //是的话将结果给出并记录
            //否的话将结果生成supp加入QsqTemplate中
            if(lieuSupp==rule.getBody().size()){
                ArrayList<Tuple>tmp;
                Tuple res;
                for(Tuple tup:nouvValDansRelationSub){
                    tmp=temp.getT();
                    //取倒数第二个supp relation 中的 tuple
                    res=tmp.get(tmp.size()-1);

                    HashMap<Value,String> hashTmp=tup.getElts();
                    //将其与最新结果结合，放入result中
                    for (Map.Entry<Value,String> entry : hashTmp.entrySet()){
                        res.ajouterUnElement(entry.getKey(),entry.getValue());
                    }
                    state.ajouterResultat(res);
                }
            }else{
                //System.out.println(temp.getT());
                //否的话将结果生成supp加入QsqTemplate中
                for(Tuple tup:nouvValDansRelationSub){
                    ArrayList<Tuple>t;
                    Tuple res;
                    t=temp.getT();
                    //取倒数第二个supp relation 中的 tuple
                    res=t.get(t.size()-1).copy();
                    HashMap<Value,String> hashTmp=tup.getElts();
                    //将其与最新结果结合，生成新的supp加入到QsqTemplate temp中
                    for (Map.Entry<Value,String> entry : hashTmp.entrySet()){
                        res.ajouterUnElement(entry.getKey(),entry.getValue());
                    }
                    //temp.ajouterTuple(res);
                    //lieuSupp++;
                    passerSuppProchaine(rule,newInput,state,temp.copy(),res,lieuSupp,indiceRule,queryOrigin);
                }
                //为什么分开了，有问题
                //System.out.println(temp.getT());
            }

        }else{
            //si les atom de type idb
            //on calculer nouveau input realtion
            //pour premier etape on faire rien pour relation input
            //theoriquement premier atom est tjr edb
            passerEDBProchaine(rule, newInput, state,indiceRule,queryOrigin);
        }
        //System.out.println(state.resultat);

    }

    /**
     * pour passer au etape suivant
     * 为了进入下一项存在
     * @param tupleNouv nouveau relation supplenmantaire
     * */
    public void passerSuppProchaine(AdornedTgd rule, Relation newInput, QSQRState state,QsqTemplate suppIncedence,Tuple tupleNouv,int lieuSupp,int indiceRule,int queryOrigin){
        //update pour relation supplementaire
        suppIncedence.ajouterTuple(tupleNouv);
        lieuSupp++;
        Atom a=suppIncedence.getListAtom().get(lieuSupp-1).atom;
        boolean typeAtomEDB=bdd.chercheConstant(a.getName());
        if(typeAtomEDB){
            ArrayList<Tuple> nouvValDansRelationSub = suppIncedence.genSupp(lieuSupp, bdd);
            //System.out.println(nouvValDansRelationSub);

            //是否是body的末端
            //是的话将结果给出并记录
            //否的话将结果生成supp加入QsqTemplate中
            if(lieuSupp==rule.getBody().size()){
                ArrayList<Tuple>tmp;
                Tuple res;
                for(Tuple tup:nouvValDansRelationSub){
                    tmp=suppIncedence.getT();
                    //取倒数第二个supp relation 中的 tuple
                    res=tmp.get(tmp.size()-1);

                    HashMap<Value,String> hashTmp=tup.getElts();
                    //将其与最新结果结合，放入result中
                    for (Map.Entry<Value,String> entry : hashTmp.entrySet()){
                        res.ajouterUnElement(entry.getKey(),entry.getValue());
                    }
                    state.ajouterResultat(res);
                }
            }else{
                System.out.println(suppIncedence.getT());
                //否的话将结果生成supp加入QsqTemplate中
                for(Tuple tup:nouvValDansRelationSub){
                    ArrayList<Tuple>t;
                    Tuple res;
                    t=suppIncedence.getT();
                    //取倒数第二个supp relation 中的 tuple
                    res=t.get(t.size()-1);
                    res=res.copy();
                    HashMap<Value,String> hashTmp=tup.getElts();
                    //将其与最新结果结合，生成新的supp加入到QsqTemplate temp中
                    for (Map.Entry<Value,String> entry : hashTmp.entrySet()){
                        res.ajouterUnElement(entry.getKey(),entry.getValue());
                    }
                    //temp.ajouterTuple(res);
                    //lieuSupp++;
                    passerSuppProchaine(rule,newInput,state,suppIncedence.copy(),res,lieuSupp,indiceRule,queryOrigin);
                }
                //System.out.println(temp.getT());
            }
        }else{
            //si les atom de type idb
            //on calculer nouveau input realtion
            //System.out.println();
            //System.out.println(suppIncedence.getT());
            //System.out.println(newInput.atomAd);
            //System.out.println(state.getAdTgd().get(indiceRule));
            //System.out.println(lieuSupp);
            //System.out.println();
            Relation renouvInput=renouvoulerRelationTnput(newInput,indiceRule,suppIncedence,state,lieuSupp);
            System.out.println(renouvInput);
            passerEDBProchaine(rule, renouvInput, state,indiceRule,queryOrigin);
        }
    }
    /**
     * renouvouler input relation
     * */
    public Relation renouvoulerRelationTnput(Relation r,int indiceRule,QsqTemplate suppIncedence,QSQRState state,int lieuSupp){
        Tuple compleMaintnance=suppIncedence.getT().get(lieuSupp-1);
        AdornedTgd tgdIdb= state.getAdTgd().get(indiceRule);
        AdornedAtom aromIdb=tgdIdb.getAtom(lieuSupp-1);
        String predicat=aromIdb.getPredicat();
        //System.out.println(predicat);
        Relation res=r.copy();
        //obtient la partie map dans relation input
        HashMap<AdornedAtom,Tuple>atomAdSchme = res.getAtomAd();
        HashMap<Integer,String>positionVariableBound = new HashMap<>();
        //cherche les nouveau vriable bound ou est dans adom de rule.
        // avec la position. on les stocker
        //System.out.println(tgdIdb);
        //System.out.println(aromIdb);
        for(int i=0;i<aromIdb.getAtom().getArgs().length;i++){
            if(compleMaintnance.chercheValue(aromIdb.getAtom().getArgs()[i])){
                positionVariableBound.put(i,compleMaintnance.chercheValeurValue(aromIdb.getAtom().getArgs()[i]));
            }
        }
        HashMap<Value,String>elts=null;
        //System.out.println(positionVariableBound);
        //parcour la partie map dans relation input
        //trouve la partie avec le predicat on renouvouler
        for (Map.Entry<AdornedAtom,Tuple> entry : atomAdSchme.entrySet()) {
            AdornedAtom key=entry.getKey();
            //si on trouve avec predicat dans rule adroned
            //on le renouvouler
            if(key.getAtom().getName().equals(predicat)){
                elts=new HashMap<>();
                for(int i=0;i<key.getAtom().getArgs().length;i++){
                    if(positionVariableBound.containsKey(i)){
                        elts.put(key.getAtom().getArgs()[i],positionVariableBound.get(i));
                    }
                }
                //System.out.println(elts);
                //renouvouler la resultat
                //System.out.println(res);
                res.renouvoulerAtomAd(predicat,new Tuple(elts));
            }
        }
        //if(elts!=null)


        return res;
    }

    /**
     * pour passer au etape suivant
     * 为了进入下一项存在
     * @param newInput nouveau iput que on obtient
     * */
    public void passerEDBProchaine(AdornedTgd rule, Relation newInput, QSQRState state,int indiceRule,int queryOrigin){
        ArrayList<AdornedTgd>arryRule = state.getAdTgd();
        //System.out.println(QSQRState1.getindiceAdTgd(p.atom));
        //parcour pour tous les rules
        //System.out.println(QSQRState1.inputDansHis(newInput));
        //System.out.println(newInput);
        //这里出问题了
        if(!QSQRState1.inputDansHis(newInput)){
            QSQRState1.ajouterHisatory(newInput);
            for(int i=0;i<arryRule.size();i++){
                //etpe 2 part 4
                //pour le rule rest
                if(newInput.getIndiceRule()!=i){
                    //faire projection et creer premier rule
                    qsqrSubroutine(arryRule.get(i), newInput, state,i,indiceRule);
                }
            }
        }
    }

    /**
     * initialiser QSQRState
     */
    public void iniIterateur(Map<Object,Object> unadornedRules,Collection edb){
        QSQRState1 = new QSQRState(unadornedRules);
        QSQRState1.iniAdornedRules(edb);
        if(QSQRState1.getAdTgd()==null){
            System.out.println("pas ini");
        }
    }

    /*getter*/
    public QSQRState getteQSQRState(){
        return QSQRState1;
    }

    //afficherQSQRState
    public void afficherAdornedRules(){
        QSQRState1.afficherAdornedRules();
    }

    /**
     * cherche NewInput d'apres le query avec ordre de query
     * */
    public Relation chercheNewInput(Atom q){
        ArrayList<AdornedTgd> rules = QSQRState1.getAdTgd();
        AdornedTgd temp;
        AdornedAtom head,atomDansBody,adomDansRealtion;
        List<AdornedAtom> body;
        ArrayList<Boolean> adornmentBodyAtom;
        int nbArg;
        //parcour tous les rules
        HashMap<Value,String>listValueBound;
        Relation res = new Relation();
        int indiceRule=0;
        for (AdornedTgd rule : rules) {
            temp = rule;
            head = temp.getHead();
            body = temp.getBody();
            //si on le trouve AdornedTgd qui a head q
            if (head.getAtom().getName().equals(q.getName())) {

                //parcour body chercher param bound
                for (AdornedAtom adornedAtom : body) {
                    atomDansBody = adornedAtom;
                    nbArg = atomDansBody.getAtom().getArgs().length;
                    listValueBound=new HashMap<>();
                    //si body atom est edb on ajouter ce relation
                    //parcours les argument dans adornedAtom
                    if (bdd.chercheConstant(atomDansBody.getAtom().getName())) {
                        for(int j=0;j<nbArg;j++){
                            adornmentBodyAtom = (ArrayList<Boolean>) atomDansBody.getAdornment();
                            adomDansRealtion = atomDansBody;

                            //parcour ts les param de ce atom dans body
                            //chercher les paramBound
                            for(int k=0;k<adornmentBodyAtom.size();k++){
                                //si c'est bound on ajouter dans tab value
                                if(adornmentBodyAtom.get(k)){
                                    listValueBound.put(atomDansBody.getAtom().getArgs()[k], bdd.chercheConstant(atomDansBody.getAtom().getName(),k));
                                }
                            }
                            Tuple resTuple = new Tuple(listValueBound);
                            res.ajouterUnTuple(resTuple);
                            HashMap<AdornedAtom,Tuple> atomAd = res.getAtomAd();
                            if(!atomAd.containsKey(adomDansRealtion)){
                                res.ajouterDansAtomAd(adomDansRealtion,resTuple);
                                //adomDansRealtion = null;
                            }
                        }
                        //si ce atom est dans idb
                        //parcour tous les argument et cherche argument existe
                    }else if(bdd.chercheIbd(atomDansBody.getAtom().getName())){

                        for(int j=0;j<nbArg;j++){
                            adornmentBodyAtom = (ArrayList<Boolean>) atomDansBody.getAdornment();
                            adomDansRealtion = atomDansBody;
                            //parcour ts les param de ce atom dans body
                            //chercher les paramBound
                            for(int k=0;k<adornmentBodyAtom.size();k++){
                                //si c'est bound on ajouter dans tab value
                                if(adornmentBodyAtom.get(k)){
                                    listValueBound.put(atomDansBody.getAtom().getArgs()[k],res.chercheTupleIBDVal(atomDansBody.getAtom().getArgs()[k]));
                                }
                            }
                            Tuple resTuple = new Tuple(listValueBound);

                            HashMap<AdornedAtom,Tuple> atomAd = res.getAtomAd();
                            if(!atomAd.containsKey(adomDansRealtion)){
                                res.ajouterDansAtomAd(adomDansRealtion,resTuple);
                                //adomDansRealtion = null;
                            }

                        }
                    }
                }

                res.setIndiceRule(indiceRule);
            }
            indiceRule++;
        }

        return res;
    }


}
