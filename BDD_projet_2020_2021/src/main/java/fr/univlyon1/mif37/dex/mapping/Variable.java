package fr.univlyon1.mif37.dex.mapping;

import java.util.Objects;

/**
 * Created by ecoquery on 20/05/2016.
 */
public class Variable implements Value {

    private String name;


    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variable variable = (Variable) o;
        return name.equals(variable.name);
    }

}
