package fr.univlyon1.mif37.dex.mapping.topDown;

import fr.univlyon1.mif37.dex.mapping.AbstractRelation;
import fr.univlyon1.mif37.dex.mapping.Atom;
import fr.univlyon1.mif37.dex.mapping.Literal;
import fr.univlyon1.mif37.dex.mapping.Value;
import fr.univlyon1.mif37.dex.mapping.topDown.AdornedAtom;
import fr.univlyon1.mif37.dex.mapping.topDown.AdornedTgd;

import java.util.*;

/**
 * A container for tracking global information passed back and forth between
 * recursion frames.
 *
 */
/*
 * 用于跟踪在之间来回传递的全局信息的容器
 * 递归帧
 * */
// changer private par private static
public class QSQRState {
    /**
     * Tracks the answer tuples generated for each adorned predicate.
     * 跟踪为每个修饰谓词生成的答案元组
     */
    public ArrayList<Tuple> resultat;
    //public Map<Object,Object> ans;
    /**
     * Tracks which input tuples have been used for each rule.
     *  跟踪每个规则使用了哪些输入元组。
     *  d'apres etape 2 part 4
     */
    public Map<Object,Object> inputByRule;




    /**
     * canal stocker les relation input qui deja utilise
     * */
    public ArrayList<Relation> historyInput;

    /**
     * Holds all the adorned rules for a given adorned predicate.
     */
    /*
     * 保存给定修饰谓词的所有修饰规则。
     * */
    private Map<Object,Object> adornedRules;
    // Holds all the adorned rules for a given adorned predicate.
    //mais en array list
    private ArrayList <AdornedTgd>AdTgd;
    /**
     * Holds all the unadorned rules for a given predicate.
     */
    /*
     * 保存给定谓词的所有简单规则。
     * */
    private final Map<Object,Object>  unadornedRules;


    /**
     * Initializes state with a set of all unadorned rules for the program.
     *
     * @param unadornedRules
     *            set of unadorned rules
     */
    /*
     * 使用程序的一组所有未修饰的规则初始化状态。
     * 未加修饰的集合
     * */
    //public QSQRState(Map<Object,Object> unadornedRules) ancien version
    public QSQRState(Map<Object,Object> unadornedRules) {
        this.resultat = new ArrayList<>();
        this.inputByRule = new LinkedHashMap<>();
        this.adornedRules = new LinkedHashMap<>();
        this.unadornedRules = unadornedRules;
        this.historyInput = new ArrayList<>();
        AdTgd = new ArrayList<>();
    }

    public ArrayList<Relation> getHistoryInput() {
        return historyInput;
    }

    public void setHistoryInput(ArrayList<Relation> historyInput) {
        this.historyInput = historyInput;
    }

    /**
     * tester input realation deja utilise
     * sion return false
     * */
    public boolean inputDansHis(Relation r){
        for(Relation i:historyInput){
            if(i.testeContient(r))
                return true;
        }
        return false;
    }

    /**
     * ajouter input realation dans his
     *
     * */
    public void ajouterHisatory(Relation r){
        historyInput.add(r);
    }

    /**
     ini AdornedRules
     */
    public void iniAdornedRules(Collection edb){
        //etape 1. creer adorned Rules d'apres unadornedRules
        // Il contient AdornedAtom
        //parcour list unadornedRules au debut
        //normalement il est tjr variable free pour partie left
        Set<Map.Entry<Object, Object>> set = unadornedRules.entrySet();
        Iterator<Map.Entry<Object, Object>> iterator = set.iterator();
        ArrayList<AdornedTgd>ResAdornedRules = new ArrayList<>();

        while(iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            //partie gauche
            Object key = entry.getKey();
            //partie droite
            Object value =  entry.getValue();
            //key est tgd left Set<Literal> (body)
            //value est tgd right Atom (head)
            Set<Literal> left = (Set<Literal>)key;
            Atom right = (Atom) value;
            Iterator i = left.iterator();
            ResAdornedRules.add(chercherBunding(left,right,edb));
        }

        //System.out.println(ResAdornedRules.size());

        for(AdornedTgd i:ResAdornedRules){
            //System.out.println(i);
            //stocker adorned rules
            adornedRules.put(i.getHead(),i.getBody());
            AdTgd.add(i);
        }
    }

    //afficher AdornedRules
    public void afficherAdornedRules(){
        Set<Map.Entry<Object, Object>> set = adornedRules.entrySet();
        Iterator<Map.Entry<Object, Object>> iterator = set.iterator();

        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    /**
     * cherche bounding avec un unadornedRules
     * */
    //nomConstant nom de predicat tjr bound
    //nomDeQuery nom de trj free
    public AdornedTgd chercherBunding(Set<Literal> body, Atom head,Collection edb){
        //stock param nom qui est bound
        Set<String>mapBound=new HashSet();
        Map<Object, Object>res=new LinkedHashMap();
        //predicat qui a tous les para, en bound

        Literal tmpL;
        Atom tmpA;
        HashMap <String, Value> nomDeVarBound;
        AdornedAtom resHead;

        HashSet<String> listPredicatEDB = chercheConstant((ArrayList<fr.univlyon1.mif37.dex.mapping.Relation>)edb);

        //parcour head qui est tjr idb
        Value headValue[] = head.getArgs();
        int nbArg=head.getArgs().length;
        ArrayList<Boolean> adornmentHead = new ArrayList<>();
        for(int i=0;i<nbArg;i++){
            if(nbArg==1){
                //un idb avec une param
                adornmentHead.add(false);
            } else if(i == 0){
                //un idb avec plusieur param
                adornmentHead.add(true);
                //ajouter premier param dans list param bound
                mapBound.add(headValue[i].getName());
            }else {
                adornmentHead.add(false);
            }
        }
        resHead = new AdornedAtom(head,adornmentHead);


        //parcour body par chaque atom si il a prediact dans edb alors tous param est tjr bound
        //si il a prediact dans idb on le detecter
        Iterator<Literal> i = body.iterator();
        ArrayList <AdornedAtom>resBody =new ArrayList<>();
        Value bodyvalue[];
        while (i.hasNext()){
            tmpL = i.next();
            tmpA = tmpL.getAtom();

            bodyvalue = tmpA.getArgs();
            nbArg = tmpA.getArgs().length;
            ArrayList<Boolean> adornmentBodyAtom = new ArrayList<>();

            //parcours tous les parametre
            for(int j=0;j<nbArg;j++){
                if (listPredicatEDB.contains(tmpA.getName())) {
                    //body atom est edb
                    adornmentBodyAtom.add(true);
                    if(!mapBound.contains(bodyvalue[j].getName())){
                        mapBound.add(bodyvalue[j].getName());
                    }
                }else{
                    if(mapBound.contains(bodyvalue[j].getName())){
                        adornmentBodyAtom.add(true);
                    }else{
                        adornmentBodyAtom.add(false);
                    }
                }
            }
            resBody.add(new AdornedAtom(tmpA,adornmentBodyAtom));
        }


        return new AdornedTgd(resHead,resBody);
    }


    /**
     cherche predicat dans edb.
     les predicat dans edb ont la premier parametre est tjr bound.
     soit constant soit fixee pqr defqut
     */
    public  HashSet<String> chercheConstant(ArrayList<fr.univlyon1.mif37.dex.mapping.Relation> edb){
        HashSet<String> res= new HashSet<>();
        for(fr.univlyon1.mif37.dex.mapping.Relation i:edb){
            if(!res.contains(i.getName())){
                res.add(i.getName());
            }
        }
        return res;
    }

    /*
   cherche predicat dans idb.
   les predicat dans idb peut dans la tete ou corp
   */
    public  HashSet<String> chercheIdb(ArrayList<AbstractRelation> idb){
        HashSet<String> res= new HashSet<>();
        for(AbstractRelation i:idb){
            if(!res.contains(i.getName())){
                res.add(i.getName());
            }
        }
        return res;
    }

    /**
     * cherche un atome adorent parmi adorentTgd
     * sinon return null
     * @param indiceAtom ordre d'atom dans rule
     * */
    public AdornedAtom chercheAtomeAdorent(int indiceRule,int indiceAtom){
        if(indiceRule<this.AdTgd.size()){
            AdornedTgd temp=this.AdTgd.get(indiceRule);
            return temp.getAtom(indiceAtom);
        }
        return null;
    }

    /*getter et setter*/

    public Map<Object, Object> getAdornedRules() {
        return adornedRules;
    }

    public void setAdornedRules(Map<Object, Object> adornedRules) {
        this.adornedRules = adornedRules;
    }

    public Map<Object, Object> getUnadornedRules() {
        return unadornedRules;
    }

    public ArrayList<AdornedTgd> getAdTgd() {
        return AdTgd;
    }

    public void setAdTgd(ArrayList<AdornedTgd> adTgd) {
        AdTgd = adTgd;
    }

    public Map<Object, Object> getInputByRule() {
        return inputByRule;
    }

    public void setInputByRule(Map<Object, Object> inputByRule) {
        this.inputByRule = inputByRule;
    }

    /**
     * cherche un head atom adorned avec un atom non adoened
     * */
    public AdornedAtom chercheAdornedAtom(Atom a){
        for(int i=0;i<AdTgd.size();i++){
            if(AdTgd.get(i).getHead().getAtom().getName().equals(a.getName())){
                if(AdTgd.get(i).getHead().getAtom().equals(a)){
                    return AdTgd.get(i).getHead();
                }
            }
        }
        return null;
    }

    /**
     * cherche rule adorned par un atom
     * */
    public AdornedTgd chercheAdornedRule(Atom q){
        for(AdornedTgd i:this.AdTgd){
            if(i.getHead().getAtom().getName().equals(q.getName()) && i.getHead().getAtom().getArgs().length==q.getArgs().length)
                return i;
        }
        return null;
    }

    /*
     * comparer et ajouter nouveau resultat
     * */
    public void ajouterResultat(Tuple a) {
        HashMap<Value,String> elts=new HashMap<>();
        elts.putAll(a.getElts());
        resultat.add(new Tuple(elts));
    }

    /**
     * find indice AdTgd  with a predicatAtom with head
     * @return -1 if not find
     * */
    public int getindiceAdTgd(Atom a){
        for(int i=0;i<AdTgd.size();i++){
            if(AdTgd.get(i).getHead().getAtom().getName().equals(a.getName())){
                return i;
            }
        }
        return -1;
    }
}