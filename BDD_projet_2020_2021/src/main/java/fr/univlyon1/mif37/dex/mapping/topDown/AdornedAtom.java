package fr.univlyon1.mif37.dex.mapping.topDown;
import fr.univlyon1.mif37.dex.mapping.Atom;
import fr.univlyon1.mif37.dex.mapping.Value;

import java.util.List;
/**
 * @juba BDD
 */

/**
 * An adorned atom. Each argument of any atom formed from this
 * predicate symbol will be marked as bound or free in accordance with the
 * predicate's adornment.
 *
 */
/*
* 一个装饰的原子。 由此形成的任何原子的每个参数
*  谓词符号将根据谓词的装饰被标记为绑定或自由。
* */

public class AdornedAtom {

    public Atom atom;
    /**
     * The adornment of the atom. A true value means that the
     * corresponding term in the arguments of an atom
     * is considered bound; a false value implies that the argument is
     * free.
     */
    /*
    * 原子的装饰。 真值意味着原子参数中的相应项被认为是绑定的； false 值意味着该参数是自由的。
     * */
    public List<Boolean> adornment;
    /**
     * Total number of bound terms in the adornment.
     */
    public int bound;
    /**
     * Constructs an adorned Atom from a atom and an
     * adornment.
     *
     * @param atom
     *            atom
     * @param adornment
     *            adornment
     */
    public AdornedAtom(Atom atom, List<Boolean> adornment) {
        //avant step 1 part 4
        //We create the adorned program by recursively creating adorned rules for all adorned predicates
        this.atom = atom;
        this.adornment=adornment;
        bound = 0;
        //compter les terms number en bound
        //son valeur est true
        for(Boolean i : adornment){
            if(i)
                bound++;
        }
    }

    /*les getter et setter pour valeur privie*/
    public Atom getAtom() {
        return atom;
    }

    public void setAtom(Atom atom) {
        this.atom = atom;
    }

    public List<Boolean> getAdornment() {
        return adornment;
    }

    public void setAdornment(List<Boolean> adornment) {
        this.adornment = adornment;
    }

    public int getBound() {
        return bound;
    }

    public void setBound(int bound) {
        this.bound = bound;
    }

    //get nom de atom
    public String getPredicat(){
        return atom.getName();
    }

    //get variable avec indice de atom
    public Value getVarDansAtom(int i){
        if(atom.getArgs().length>i){
            return atom.chercheValue(i);
        }else{
            return null;
        }
    }

    @Override
    public String toString() {
        return "AdornedAtom{" +
                "atom=" + atom +
                ", adornment=" + adornment +
                ", bound=" + bound +
                '}';
    }
}
