package fr.univlyon1.mif37.dex;

import fr.univlyon1.mif37.dex.mapping.Atom;
import fr.univlyon1.mif37.dex.mapping.Literal;
import fr.univlyon1.mif37.dex.mapping.Mapping;


import fr.univlyon1.mif37.dex.mapping.Tgd;
import fr.univlyon1.mif37.dex.mapping.topDown.QSQRState;
import fr.univlyon1.mif37.dex.mapping.topDown.RecursiveQsqEngine;
import fr.univlyon1.mif37.dex.mapping.topDown.Relation;
import fr.univlyon1.mif37.dex.mapping.topDown.Tuple;
import fr.univlyon1.mif37.dex.parser.MappingParser;
import fr.univlyon1.mif37.dex.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class App {
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws Exception {
        MappingParser mp = new MappingParser(App.class.getResourceAsStream("/exemple1.2.txt"));
        Mapping mapping = mp.mapping();
        LOG.info("\n"+mapping.toString());
        LOG.info("Parsed {} edb(s), {} idb(s) and {} tgd(s).",
                mapping.getEDB().size(),
                mapping.getIDB().size(),
                mapping.getTgds().size());

        //tester mapping bien charger les donnes
        //construit unadornedRules d'pres list tgd
        /*ArrayList<Tgd> tgdOrigin= (ArrayList)mapping.getTgds();
        Map < Object,Object>unadornedRules = new LinkedHashMap();
        Tgd tempT;
        for(int i=0;i<tgdOrigin.size();i++){
            tempT = tgdOrigin.get(i);
            unadornedRules.put(tempT.getLeft(),tempT.getRight());
        }*/

        RecursiveQsqEngine r=new RecursiveQsqEngine(mapping);
        r.afficherAdornedRules();
        Atom qurey = mapping.chercheQuery(0);
        r.query(qurey);
        System.out.println("Resultat dans engine"+r.getteQSQRState().resultat);
        //ameliorerResultat(r.getteQSQRState().resultat,qurey);

    }

    public static void ameliorerResultat(ArrayList<Tuple> resultat,Atom a){
        ArrayList<String>res=new ArrayList<>();
        for(Tuple i:resultat){
            //normalment il y a suel param pour query
            if(a.getArgs().length==1){
                if(i.chercheValue(a.chercheValue(0))){
                    res.add(i.chercheValeurValue(a.chercheValue(0)));
                }
            }
        }
        System.out.println("result of query : "+a+" is "+res);
    }

}
